# Custom fish prompt based on tomita with git stuff from trout

function _git_branch_name # get the current branch name from git
    echo (command git symbolic-ref HEAD 2> /dev/null | sed -e 's|^refs/heads/||')
end

function _is_git_dirty # check if current git branch is dirty
    echo (command git status -s --ignore-submodules=dirty 2> /dev/null)
end

function _is_git_up_to_date # check if any git commits have to be pushed
    echo (command git log origin/(_git_branch_name)..HEAD 2> /dev/null)
end

function fish_prompt
    set -l tomita_vi_mode "$TOMITA_VI"

    # print shortened path
	set_color $fish_color_cwd
    printf '%s ' (prompt_pwd)
    set_color normal

    if [ (_git_branch_name) ] # check if we are in a git repo
        
        printf '%s' (_git_branch_name) # print branch name

        if [ (_is_git_dirty) ] # check if git is dirty
            set_color --bold red
        else if [ (_is_git_up_to_date) ] 
            set_color --bold yellow
        else
            set_color --bold green
        end
        printf '* '
        set_color normal
    end


    if test -z (string match -ri '^no|false|0$' $tomita_vi_mode)
        
        switch $fish_bind_mode
            case default
                set_color --bold red
            case insert
                set_color --bold yellow
            case visual
                set_color --bold magenta
        end
    end
    
    if test (id -u) -eq 0
        printf '# '
    else
        printf '> '
    end

    set_color normal
end

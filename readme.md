# Fishy-Theme

Minimal, informative, fishy

Based on [tomita](https://github.com/daveyarwood/tomita) and [trout](https://github.com/oh-my-fish/theme-trout)

### Features

* Git
    - Branch labeling
    - Status
* Root Detection
* VI mode

